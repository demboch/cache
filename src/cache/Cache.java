package cache;

import java.util.*;

public class Cache {

	private static Cache instance;
	private Map<EnumerationName, ArrayList<EnumValue>> map;

	private Cache() {
		this.map = new HashMap<EnumerationName, ArrayList<EnumValue>>();
	}

	public void put(EnumerationName cacheKey, EnumValue value) {
		synchronized (this) {
			if (!this.map.containsKey(cacheKey)) {
				this.map.put(cacheKey, new ArrayList<EnumValue>());
			}
			this.map.get(cacheKey).add(value);
		}
	}

	public EnumValue search(EnumerationName cacheKey, int valueId) {
		synchronized (this) {
			ArrayList<EnumValue> list = instance.map.get(cacheKey);
			for (EnumValue ev : list) {
				if (ev.getId() == valueId)
					return ev;
			}
			return null;
		}
	}

	public void addToCache(Map<EnumerationName, ArrayList<EnumValue>> addedElements) {
		synchronized (this) {
			for (EnumerationName key : addedElements.keySet()) { // wyciagamy
																	// wszystkie
																	// klucze
				ArrayList<EnumValue> newElements = addedElements.get(key);
				if (this.map.containsKey(key)) {
					ArrayList<EnumValue> targetList = this.map.get(key);
					targetList.addAll(newElements);
				} else {
					this.map.put(key, new ArrayList<EnumValue>());
					this.map.get(key).addAll(newElements);
				}
			}
		}
	}
	
	public static Cache getInstance() {
		synchronized (Cache.class) {
			if (instance == null)
				instance = new Cache();
			return instance;
		}
	}
	
	public ArrayList<EnumValue> get(EnumerationName cacheKey) {
		synchronized (this) {
			return this.map.get(cacheKey);
		}
	}

	public Map<EnumerationName, ArrayList<EnumValue>> getInnerMap() {
		synchronized (this) {
			return this.map;
		}
	}
	
	public void clear() {
		synchronized (this) {
			this.map.clear();
		}
	}
}
