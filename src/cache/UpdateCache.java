package cache;

import java.util.*;

public class UpdateCache implements Update, Runnable {

	private long lifetime = 6000;
	private Cache cache = Cache.getInstance();
	private Map<EnumerationName, ArrayList<EnumValue>> map;

	@Override
	public void CacheData(Map<EnumerationName, ArrayList<EnumValue>> map) {
		this.map = map;
	}

	@Override
	public synchronized void run() {
		while (true) {
			operation();
			try {
				Thread.sleep(lifetime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void operation() {
		System.out.println("BEFORE ADDING TO CACHE");
		cache.addToCache(map);
		System.out.println(Cache.getInstance().getInnerMap());
	}
}
