package cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Test {

	public static void main(String[] args) {
		
		Cache cache = Cache.getInstance();

		UpdateCache update = new UpdateCache();
		
		cache.put(EnumerationName.SERVICE, new EnumValue(1, 1, "CL", "Cleaning", EnumerationName.SERVICE));
		cache.put(EnumerationName.GENDER, new EnumValue(2, 2, "M", "Maciej", EnumerationName.GENDER));
		cache.put(EnumerationName.REGION, new EnumValue(3, 3, "PO", "Pomorskie", EnumerationName.REGION));	
		
		System.out.println(cache.getInnerMap());
		
		/////////////////////////////////////////////////////////////////////////////////////////////////
		
		Map<EnumerationName, ArrayList<EnumValue>> map = new HashMap<EnumerationName,ArrayList<EnumValue>>();
		ArrayList<EnumValue> line1 = new ArrayList<EnumValue>();
		line1.add(new EnumValue(4, 1, "RE", "Reapir", EnumerationName.SERVICE));
		map.put(EnumerationName.SERVICE, line1);
		ArrayList<EnumValue> line2 = new ArrayList<EnumValue>();
		line2.add(new EnumValue(6, 3, "MA", "Mazowieckie", EnumerationName.REGION));
		map.put(EnumerationName.REGION, line2);	
		
		update.CacheData(map); // aktualizowanie cache
		
		Thread thread1 = new Thread(update);	// w�tek 1
		thread1.run();	
		//System.out.println(cache.getInnerMap());
		
		ArrayList<EnumValue> line3 = new ArrayList<EnumValue>();
		line3.add(new EnumValue(5, 2, "K", "Maja", EnumerationName.GENDER));
		map.put(EnumerationName.GENDER, line3);
		
		Thread thread2 = new Thread(update); // w�tek 2
		thread2.run();
		//System.out.println(cache.getInnerMap());
		
		//cache.clear(); // czysczeinie cache
		
		//System.out.println(cache.getInnerMap());
		
		try {
			thread1.join();
			thread2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("KONIEC PROGRAMU");
	
	}
}
