package cache;

import java.util.ArrayList;
import java.util.Map;

public interface Update {

	public void CacheData(Map<EnumerationName,ArrayList<EnumValue>> map);
	public void operation();
}