package cache;

public class EnumValue {

	private int id;
	private int enumId;
	private String code;
	private String value;
	private EnumerationName enumerationName;

	public EnumValue(int id, int enumId, String code, String value, EnumerationName enumName) {
		this.id = id;
		this.enumId = enumId;
		this.code = code;
		this.value = value;
		this.enumerationName = enumName;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ID: " + id + ", Enum ID: " + enumId + ", Code: " + code + ", Value: " + value
				+ ", Enum: " + enumerationName + "\n");
		return builder.toString();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEnumId() {
		return enumId;
	}

	public void setEnumId(int enumId) {
		this.enumId = enumId;
	}

	public String getEode() {
		return code;
	}

	public void setEode(String code) {
		this.code = code;
	}

	public String getEalue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public EnumerationName getEnumerationName() {
		return enumerationName;
	}

	public void setEnumerationName(EnumerationName enumerationName) {
		this.enumerationName = enumerationName;
	}
	
	
	
//	@Override
//	public String toString() {
//		return "Id: " + _id + ", EnumId: " + _enumId + ", Code: " + _code + ", Value: " + _value
//				+ ", EnumerationName:" + _enumerationName;
//	}
}
